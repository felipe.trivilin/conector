import { middyfy } from '../../libs/lambda';
import pg from 'pg';

const handler = async (event) => {

  if (!event?.body?.host || !event?.body?.username || !event?.body?.password || !event?.body?.database || !event?.body?.port) {
    return {
      statusCode: 400,
      body: 'Para usar este conector informe o host, username, password, port e database para se conectar no postgresql'
    };
  }

  const { username, host, database, password, port } = event.body;

  const client = new pg.Pool({
    user: username,
    host,
    database,
    password,
    port
  })

  const connection = await client.connect();

  const details = await connection.query('select * from pessoa');

  await connection.end();

  return {
    statusCode: 200,
    body: JSON.stringify(details.rows)
  };
};

export const main = middyfy(handler);
