import mysql from 'mysql2/promise';

import { middyfy } from '../../libs/lambda';

const handler = async (event) => {

  if (!event?.body?.host || !event?.body?.username || !event?.body?.password || !event?.body?.database) {
    return {
      statusCode: 400,
      body: 'Para usar este conector informe o host, username, password e database para se conectar no mysql'
    };
  }

  const body = event.body;

  const connection = await mysql.createConnection({
    host: body.host,
    user: body.username,
    password: body.password,
    database: body.database
  });

  const details = await connection.query('select * from pessoa');

  await connection.end();

  return {
    statusCode: 200,
    body: JSON.stringify(details[0])
  };
};

export const main = middyfy(handler);