import { middyfy } from '../../libs/lambda';
import mssql from 'mssql';

const handler = async (event) => {

  if (!event?.body?.server  || !event?.body?.username || !event?.body?.password || !event?.body?.database  ) {
    return {
      statusCode: 400,
      body: 'Para usar este conector informe o server, username, password e database para se conectar no sqlserver'
    };
  }

  const { server, username, password, database } = event.body;

  const config = {
    user: username,
    password,
    server, 
    database,
    trustServerCertificate: true 
  };

  await mssql.connect(config)

  const request = new mssql.Request();

  const recordset = await request.query('select * from pessoa')

  return {
    statusCode: 200,
    body: JSON.stringify(recordset)
  };

};

export const main = middyfy(handler);