# TEMPLATE CHANGELOG (ALTERAR)

# {version}
[{date}]

### Quebras de compatibilidade
* N/A.

### Novas funcionalidades
* [TECBPM-510](https://jira.senior.com.br/browse/TECBPM-510) - Criando conector para iniciar uma nova solicitação.
* [TECBPM-588](https://jira.senior.com.br/browse/TECBPM-588) - Criação conector para cancelar instância do processo.
* [TECBPM-990](https://jira.senior.com.br/browse/TECBPM-990) - Criação conector genérico HTTP para Novasoft.
* [TECBPM-2068](https://jira.senior.com.br/browse/TECBPM-2068) - Criação conector listar usuários ativos SeniorX
* [TECBPM-2653](https://jira.senior.com.br/browse/TECBPM-2653) - Criação plugins Mega
* [PDATA-53](https://megasistemas.atlassian.net/browse/PDATA-53) - Criação plugins Mega

### Melhorias
* [TECBPM-510](https://jira.senior.com.br/browse/TECBPM-510) - Criando conector para iniciar uma nova solicitação.

### Correções
* [TECDOC-1828](https://jira.senior.com.br/browse/TECDOC-1828) - Atualizando plugin de geração de envelopes em lote.
* [TECBPM-814](https://jira.senior.com.br/browse/TECBPM-814) - Ajuste no Conector que cria envelope no BPM
* [TECBPM-1566](https://jira.senior.com.br/browse/TECBPM-1566) - Ajuste no Conector que starta nova solicitação.
* [TECBPM-1973](https://jira.senior.com.br/browse/TECBPM-1973) - Ajuste no Conector G5-query-data para tratar valor vazio
* [TECBPM-2449](https://jira.senior.com.br/browse/TECBPM-2449) - Ajuste no Conector G5-query-data retornando 200 mesmo com erro na requisição para G5
* [TECBPM-2495](https://jira.senior.com.br/browse/TECBPM-2495) - Ajuste no Conector G5-insert-data retornando 200 mesmo com erro na requisição para G5


### Alterações na base de dados
* N/A.

### Alteração de dependências
* N/A.
