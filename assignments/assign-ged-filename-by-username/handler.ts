import { middyfy } from '../../libs/lambda';

import axios from 'axios';

const PATH = '/platform/ecm_ged/queries/folderListPageable';
let response;

const assignmentGedFilename = async (event) => {

  try {

    let processPdfs = [] as { id: string, title: string }[];
    const url = event.headers['x-platform-environment'] + PATH;
    for (const folder of event.body.folder.split(';')) {
      const files = await axios.get(`${url}?id=${folder}&page=0&size=10000&onlyFolders=false`, {
        headers: { Authorization: event.headers["x-platform-authorization"] }
      });
      if (files.data?.files?.length) {
        processPdfs = processPdfs.concat(files.data.files.filter(f => f.mimetype === 'application/pdf'));
      }
    }

    const indexFile = (event.body.filenameType as string).split(event.body.separator).findIndex(text => text === event.body.positionName);

    const users = processPdfs
      .map(p => {
        return {
          documentId: p.id,
          user: p.title.split(event.body.separator).length > indexFile ? p.title.split(event.body.separator)[indexFile].replace('.PDF', '').replace('.pdf', '') : '',
          error: p.title.split(event.body.separator).length <= indexFile
        }
      });


    const scheduling = new Date();

    response = {
      statusCode: 200,
      body: JSON.stringify(
        users?.map(user => ({
          context: user,
          scheduling
        }))
      )
    };

  } catch (error) {
    console.log(error);
    response = {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message)
    };
  }

  return response;
};

export const main = middyfy(assignmentGedFilename);