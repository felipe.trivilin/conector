import middy from "@middy/core"
import middyJsonBodyParser from "@middy/http-json-body-parser"

export const middyfy = (handler) => {
  return middy(handler).use(middyJsonBodyParser())
    .after(middleware => {
      console.log('RequestId retornado', middleware.context.awsRequestId);
      if (!middleware?.response?.headers) {
        middleware.response.headers = {};
      }
      middleware.response.headers = {
        ...middleware.response.headers,
        'requestId': middleware.context.awsRequestId
      };
    })
    .onError(middleware => {
      // Sempre que ocorrer algum erro e não tiver sido tratado
      // retorna o requestId para obter os logs do CloudWatch
      console.log(middleware.error);
      console.log(middleware.context.awsRequestId);

      if (!middleware.response) {
        middleware.response = {
          statusCode: 500,
          headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': '*',
            'Access-Control-Allow-Credentials': true,
            'requestId': middleware.context.awsRequestId
          },
          body: 'Internal Server Error'
        }
      } else {
        if (!middleware?.response?.headers) {
          middleware.response.headers = {};
        }
        middleware.response.headers = {
          ...middleware.response.headers,
          'requestId': middleware.context.awsRequestId
        }
      }
    });
}
