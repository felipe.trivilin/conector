import { middyfy } from '../../libs/lambda';

import axios from 'axios';

const handler = async (event) => {

  const cep = event?.body?.cep?.replace('-', '');

  const response = await axios.get(`https://viacep.com.br/ws/${cep}/json`);

  return {
    statusCode: 200,
    body: JSON.stringify({
      cep: response?.data?.cep,
      street: response?.data?.logradouro,
      complement: response?.data?.complemento,
      province: response?.data?.bairro,
      city: response?.data?.localidade,
      uf: response?.data?.uf,
      ddd: response?.data?.ddd,
      ibge: response?.data?.ibge
    })
  };

};

export const main = middyfy(handler);