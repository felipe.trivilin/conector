/* eslint-disable  @typescript-eslint/no-explicit-any */
import { middyfy } from '../../libs/lambda';

import Axios from 'axios';

import StringMask from 'string-mask';

const PATH_EMPLOYEE = '/hcm/employeejourney/apis/employee';

const infoAllEmployee = async (event) => {
  const url = event.headers['x-platform-environment'] + PATH_EMPLOYEE;

  const filter = [] as string[];

  filter.push(`dismissalDate is null`);
        
  const employeeId = event?.queryStringParameters?.employeeId;
  const email = event?.queryStringParameters?.email;
  const contractType = event?.queryStringParameters?.contractType;
  const hireDate = event?.queryStringParameters?.hireDate;
  const dismissalDate = event?.queryStringParameters?.dismissalDate;
  const employeeType = event?.queryStringParameters?.employeeType;
  const registerNumber = event?.queryStringParameters?.registerNumber;
  const employerId = event?.queryStringParameters?.employerId;
  const employerNumemp = event?.queryStringParameters?.employerNumemp;
  const employerTradingName = event?.queryStringParameters?.employerTradingName;
  const employerCompanyName = event?.queryStringParameters?.employerCompanyName;
  const employerCnpj = event?.queryStringParameters?.employerCnpj;
  const personId = event?.queryStringParameters?.personId;
  const personFullname = event?.queryStringParameters?.personFullname;
  const personCpf = event?.queryStringParameters?.personCpf;
  const jobPositionCode = event?.queryStringParameters?.jobPositionCode;
  const jobPositionName = event?.queryStringParameters?.jobPositionName;
  const departmentCode = event?.queryStringParameters?.departmentCode;
  const departmentName = event?.queryStringParameters?.departmentName;
  const costCenterCode = event?.queryStringParameters?.costCenterCode;
  const costCenterName = event?.queryStringParameters?.costCenterName;
  const workShiftCode = event?.queryStringParameters?.workShiftCode;
  const workShiftName = event?.queryStringParameters?.workShiftName;

  if (employeeId) {
    filter.push(`id eq '${employeeId}'`);
  }

  if (email) {
    filter.push(`emails.email eq '${email}'`);
  }

  if (contractType) {
    const contractTypeFilter = getContractType(contractType);
    if (contractTypeFilter) {
      filter.push(`contractType eq '${contractTypeFilter}'`);
    }
  }

  if (hireDate) {
    filter.push(`hireDate eq '${hireDate}'`);
  }

  if (dismissalDate) {
    filter.push(`dismissalDate eq '${dismissalDate}'`);
  }

  if (employeeType) {
    const employeeTypeFilter = getEmployeeType(employeeType)
    if (employeeTypeFilter) {
      filter.push(`employeeType eq ${employeeTypeFilter}`);
    }
  }

  if (registerNumber) {
    filter.push(`registerNumber eq '${registerNumber}'`);
  }

  if (employerId) {
    filter.push(`employer.headquarter.id eq '${employerId}'`);
  }

  if (employerNumemp) {
    filter.push(`employer.headquarter.numemp eq ${employerNumemp}`);
  }

  if (employerTradingName) {
    filter.push(`containing(upper(employer.headquarter.tradingName), upper('${employerTradingName}'))`);
  }

  if (employerCompanyName) {
    filter.push(`containing(upper(employer.headquarter.companyName), upper('${employerCompanyName}'))`);
  }

  if (employerCnpj) {
    filter.push(`employer.headquarter.cnpj eq '${removePunctuation(employerCnpj)}'`);
  }

  if (personId) {
    filter.push(`person.id eq '${personId}'`);
  }

  if (personCpf) {
    filter.push(`person.cpf eq '${removePunctuation(personCpf)}'`);
  }

  if (personFullname) {
    const filterPerson = filterName(personFullname);
    if (filterPerson) {
      filter.push(filterPerson);
    }
  }

  if (workShiftCode) {
    filter.push(`workShift.codesc eq '${workShiftCode}'`);
  }

  if (workShiftName) {
    filter.push(`containing(upper(workShift.name), upper('${workShiftName}'))`);
  }

  if (costCenterCode) {
    filter.push(`costCenter.codccu eq '${costCenterCode}'`);
  }

  if (costCenterName) {
    filter.push(`containing(upper(costCenter.name), upper('${costCenterName}'))`);
  }

  if (jobPositionCode) {
    filter.push(`jobPosition.codcar eq '${jobPositionCode}'`);
  }

  if (jobPositionName) {
    filter.push(`containing(upper(jobPosition.name), upper('${jobPositionName}'))`);
  } 
  
  if (departmentCode) {
    filter.push(`department.code eq '${departmentCode}'`);
  }

  if (departmentName) {
    filter.push(`containing(upper(department.name), upper('${departmentName}'))`);
  }

  const page = event?.queryStringParameters?.page || 0;
  const size = event?.queryStringParameters?.size || 10;

  try {
    const responseData = await Axios.get(`${url}${filter.length ? '?filter=' : ''}${filter.length ? filter.join(' AND ') : ''}${filter.length ? '&' : '?'}size=${size}&offset=${page}`, {
      headers: {
        Authorization: event.headers['x-platform-authorization']
      }
    });

    const data = responseData.data;
    const contents: any[] = [];
    const response = {
      contents: contents,
      totalPages: data.totalPages,
      totalElements: data.totalElements
    }

    data.contents.forEach(employee => {
      const employeeType = getEmployeeTypeLabel(employee.employeeType);
      const contractType = getContractTypeLabel(employee.contractType);

      let cpf = '';

      if (employee.person?.cpf) {
        cpf = employee.person?.cpf;
        const formatter = new StringMask('000.000.000-00');
        if (cpf.length < 11) {
          while (cpf.length < 11) {
            cpf = '0'+cpf;
          }
        }

        cpf = formatter.apply(cpf);
      }

      let cnpj = '';

      if (employee.employer?.headquarter?.cnpj) {
        cnpj = employee.employer?.headquarter?.cnpj;
        const formatter = new StringMask('00.000.000/0000-00');
        if (cnpj.length < 14) {
          while (cnpj.length < 14) {
            cnpj = '0'+cnpj;
          }
        }

        cnpj = formatter.apply(cnpj);
      }
      

      const employeeFormated = {
        employeeId: employee.id ? employee.id : '',        
        email: employee.emails && employee.emails.length > 0 ? employee.emails[0].email : '',
        contractType: contractType,
        hireDate: employee.hireDate ? employee.hireDate : '',
        employeeType: employeeType,
        registerNumber: employee.registerNumber ? employee.registerNumber.toString() : '',
        employerId: employee.employer && employee.employer.headquarter ? employee.employer.headquarter.id : '',
        employerNumemp: employee.employer && employee.employer.headquarter ? employee.employer?.headquarter.numemp : '',
        employerTradingName: employee.employer && employee.employer.headquarter ? employee.employer.headquarter.tradingName : '',
        employerCompanyName: employee.employer && employee.employer.headquarter ? employee.employer.headquarter.companyName : '',
        employerCnpj: cnpj,
        personId: employee.person? employee.person.id : '',
        personFullname: employee.person? employee.person.fullName : '',
        personCpf: cpf,
        jobPositionCode: employee.jobPosition? employee.jobPosition.codcar : '',
        jobPositionName: employee.jobPosition? employee.jobPosition.name : '',
        departmentCode: employee.department? employee.department.code : '',
        departmentName: employee.department? employee.department.name : '',
        costCenterCode: employee.costCenter ? employee.costCenter.codccu : '',
        costCenterName: employee.costCenter ? employee.costCenter.name : '',
        workShiftCode: employee.workShift ? employee.workShift.codesc : '',
        workShiftName: employee.workShift ? employee.workShift.name : ''
      };
      
      response.contents.push(employeeFormated);
    });

    return {
      statusCode: responseData?.status || 200,
      body: JSON.stringify(response)
    };
  } catch (error) {
    console.log(error);
    return {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message)
    };
  }
};

const getEmployeeType = (employeeTypeLabel: string) => {
  let employeeType;
  switch (employeeTypeLabel) {
    case 'Empregado':
      employeeType = 1;
      break;
    case 'Terceiro':
      employeeType = 3;
      break;
    case 'Parceiro':
      employeeType = 2;
      break;
    default:
      employeeType = undefined;        
      break;
    }
    return employeeType;
}

const removePunctuation = (texto: string) => {
  return texto.replace('.', '').replace('.', '').replace('/', '').replace('-', '');
}

const getEmployeeTypeLabel = (employeeType: string) => {
  let employeeTypeLabel = '';
  switch (employeeType) {
    case 'EMPLOYEE':
      employeeTypeLabel = 'Empregado';
      break;
    case 'THIRDPARTY':
      employeeTypeLabel = 'Terceiro';
      break;
    case 'PARTNER':
      employeeTypeLabel = 'Parceiro';
      break;
    default:
      employeeTypeLabel = '';        
      break;
    }
  return employeeTypeLabel;
}

const filterName = (search: string) => {
  const nomeUsuarioSplit = search.split(' ');
  const primeiroNome = nomeUsuarioSplit.shift();
  let sobreNome = nomeUsuarioSplit.pop();
  const totalPartesDoNome = nomeUsuarioSplit.length;
  let juntarMeioDoNome = nomeUsuarioSplit.join();

  for (let count = 0; count < totalPartesDoNome; count++) {
    juntarMeioDoNome = juntarMeioDoNome.replace(',', ' ');
  }

  if (sobreNome === undefined) {
    sobreNome = '';
  }

  let filter = '';
  if (primeiroNome) {
    filter += `containing(lower(person.firstname),lower('${primeiroNome}')) `;
  }
  if (juntarMeioDoNome) {
    filter += ` and containing(lower(person.middlename),lower('${juntarMeioDoNome}')) `;
  }
  if (sobreNome) {
    filter += ` and containing(lower(person.lastname),lower('${sobreNome}')) `;
  }

  return filter;
}

const getContractTypeLabel = (contractType: string) => {
  let contractTypeLabel = '';
  switch (contractType) {
    case 'EMPLOYEE':
      contractTypeLabel = 'Empregado';
      break;
    case 'MANAGER':
      contractTypeLabel = 'Diretor';
      break;
    case 'FARM_WORKER':
      contractTypeLabel = 'Trabalhador Rural';
      break;        
    case 'RETIRED':
      contractTypeLabel = 'Aposentado';
      break;
    case 'TRAINEE':
      contractTypeLabel = 'Estagiário';
      break;
    case 'APRENDIZ':
      contractTypeLabel = 'Aprendiz';
      break;
    case 'FIXED_DUE_DATE':
      contractTypeLabel = 'Prazo Determinado - Lei 9.601/98';
      break;
    case 'RETIRED_MANAGER':
      contractTypeLabel = 'Diretor Aposentado';
      break;
    case 'PUBLIC_AGENT':
      contractTypeLabel = 'Agente Público';
      break;
    case 'TEACHER':
      contractTypeLabel = 'Professor';
      break;
    case 'COOPERATIVE_WORKER':
      contractTypeLabel = 'Cooperado';
      break;
    case 'DOMESTIC_WORKER':
      contractTypeLabel = 'Trabalhador Doméstico';
      break;
    case 'TEACHER_FIXED_DUE_DATE':
      contractTypeLabel = 'Professor Prazo Determinado';
      break;
    default:
      contractTypeLabel = '';        
      break;
  }
  return contractTypeLabel;
}

const getContractType = (contractTypeLabel: string) => {
  let contractType = '';
  switch (contractTypeLabel) {
    case 'Empregado':
      contractType = 'EMPLOYEE';
      break;
    case 'Diretor':
      contractType = 'MANAGER';
      break;
    case 'Trabalhador Rural':
      contractType = 'FARM_WORKER';
      break;        
    case 'Aposentado':
      contractType = 'RETIRED';
      break;
    case 'Estagiário':
      contractType = 'TRAINEE';
      break;
    case 'Aprendiz':
      contractType = 'APRENDIZ';
      break;
    case 'Prazo Determinado - Lei 9.601/98':
      contractType = 'FIXED_DUE_DATE';
      break;
    case 'Diretor Aposentado':
      contractType = 'RETIRED_MANAGER';
      break;
    case 'Agente Público':
      contractType = 'PUBLIC_AGENT';
      break;
    case 'Professor':
      contractType = 'TEACHER';
      break;
    case 'Cooperado':
      contractType = 'COOPERATIVE_WORKER';
      break;
    case 'Trabalhador Doméstico':
      contractType = 'DOMESTIC_WORKER';
      break;
    case 'Professor Prazo Determinado':
      contractType = 'TEACHER_FIXED_DUE_DATE';
      break;
    default:
      contractType = '';        
      break;
  }
  return contractType;
}

export const main = middyfy(infoAllEmployee);