import { middyfy } from '../../libs/lambda';

let response = {};

const sendListObject = async (event) => {
    const body = event.body;
    const lista = body.lista;
    
    response = {
        statusCode: 200,
        name: lista[0].name
    }
    
    return JSON.stringify(response);
}
export const main = middyfy(sendListObject);