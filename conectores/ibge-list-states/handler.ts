import { middyfy } from '../../libs/lambda';

const federalStates = {
  contents: [{
    federalState: 'São Paulo',
    abbreviation: 'SP'
  },
  {
    federalState: 'Paraná',
    abbreviation: 'PR'
  },
  {
    federalState: 'Santa Catarina',
    abbreviation: 'SC'
  },
  {
    federalState: 'Rio Grande do Sul',
    abbreviation: 'RS'
  },
  {
    federalState: 'Mato Grosso do Sul',
    abbreviation: 'MS'
  },
  {
    federalState: 'Rondônia',
    abbreviation: 'RO'
  },
  {
    federalState: 'Acre',
    abbreviation: 'AC'
  },
  {
    federalState: 'Amazonas',
    abbreviation: 'AM'
  },
  {
    federalState: 'Roraima',
    abbreviation: 'RR'
  },
  {
    federalState: 'Pará',
    abbreviation: 'PA'
  },
  {
    federalState: 'Amapá',
    abbreviation: 'AP'
  },
  {
    federalState: 'Tocantins',
    abbreviation: 'TO'
  },
  {
    federalState: 'Maranhão',
    abbreviation: 'MA'
  },
  {
    federalState: 'Rio Grande do Norte',
    abbreviation: 'RN'
  },
  {
    federalState: 'Paraíba',
    abbreviation: 'PB'
  },
  {
    federalState: 'Pernambuco',
    abbreviation: 'PE'
  },
  {
    federalState: 'Alagoas',
    abbreviation: 'AL'
  },
  {
    federalState: 'Sergipe',
    abbreviation: 'SE'
  },
  {
    federalState: 'Bahia',
    abbreviation: 'BA'
  },
  {
    federalState: 'Minas Gerais',
    abbreviation: 'MG'
  },
  {
    federalState: 'Rio de Janeiro',
    abbreviation: 'RJ'
  },
  {
    federalState: 'Mato Grosso',
    abbreviation: 'MT'
  },
  {
    federalState: 'Goiás',
    abbreviation: 'GO'
  },
  {
    federalState: 'Distrito Federal',
    abbreviation: 'DF'
  },
  {
    federalState: 'Piauí',
    abbreviation: 'PI'
  },
  {
    federalState: 'Ceará',
    abbreviation: 'CE'
  },
  {
    federalState: 'Espírito Santo',
    abbreviation: 'ES'
  }
]};

const ibgeListStates = async (event) => {

  const federalState = event?.queryStringParameters?.federalState;
  const abbreviation = event?.queryStringParameters?.abbreviation;

  try {

    if (federalState) {
      return {statusCode: 200, body: JSON.stringify(
        federalStates.contents.filter(content => content.federalState.includes(federalState))
      )}
    }

    if (abbreviation) {
      return {statusCode: 200, body: JSON.stringify(
        federalStates.contents.filter(content => content.abbreviation.includes(abbreviation))
      )}
    }

    return {statusCode: 200, body: JSON.stringify(federalStates)};
  } catch (error) {
    console.log(error);
    return {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message)
    };
  }
};

export const main = middyfy(ibgeListStates);