import { middyfy } from '../../libs/lambda';

import Axios from 'axios';

const PATH = '/platform/workflow/actions/cancelProcessInstance';
let response;

const cancelProcessInstance = async (event) => {

  try {

    const url = event.headers['x-platform-environment'] + PATH;
    // Transform processInstanceId into a list and add it to ids from SDL
    event.body.ids = [event.body.processInstanceId]

    const responseData = await Axios.post(url, event.body, {
      headers: { Authorization: event.headers['x-platform-authorization'] }         
    });

    response = {
      statusCode: responseData?.status || 200,
      body: JSON.stringify(responseData?.data)
    };  

  } catch (error) {
    console.log(error);
    response = {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message)
    };
  }

  return response;
};

export const main = middyfy(cancelProcessInstance);