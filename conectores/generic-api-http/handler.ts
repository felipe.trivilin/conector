/* eslint-disable  @typescript-eslint/no-explicit-any */
import { middyfy } from '../../libs/lambda';

import Axios from 'axios';

const genericApiHttp = async (event) => {

  try {
    const body = event.body;

    const url = body.url;

    const method = body.method ? body.method.toLowerCase() : 'post';

    const headers = { 'authorization': event.headers['x-platform-authorization'] }

    if (body.customHeaders) {

      const custom_header = body.customHeaders;

      const arr_custom = custom_header.split(';');

      for (let i = 0; i < arr_custom.length; i++) {
        const header = arr_custom[i].split(':');
        headers[header[0]] = header[1];
      }
      delete body.customHeaders;
    }

    delete body.url;
    delete body.method;

    let responseData = null as any;

    if (method === 'post') {
      responseData = await Axios.post(url, body, {
        headers: headers
      });
    } else if (method === 'get') {
      responseData = await Axios.get(url, {
        params: body,
        headers: headers
      });
    } else if (method === 'put') {
      responseData = await Axios.put(url, body, {
        headers: headers
      });
    } else {
      responseData = await Axios.delete(url, {
        params: body,
        headers: headers
      });
    }

    let response = responseData.data;

    if (event.body.rootObject) {
      response = response[event.body.rootObject];
    }

    return {
      statusCode: responseData?.status || 200,
      body: JSON.stringify(response)
    };

  } catch (error) {
    console.log(error);
    return {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message)
    }
  }
};


export const main = middyfy(genericApiHttp);
