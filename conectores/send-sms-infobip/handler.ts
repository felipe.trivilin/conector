import { middyfy } from '../../libs/lambda';
import axios from 'axios';
import { validatePhoneNumber } from '../../libs/utils';

const sendSms = async (event) => {
  try {
    const { destination, content, token } = event.body;

    const phoneNumber = validatePhoneNumber(destination);

    const data = {
      messages: [
        {
          destinations: [
            {
              to: phoneNumber
            }
          ],
          text: content
        }
      ]
    }


    const result = await axios.post('https://pwldee.api.infobip.com/sms/2/text/advanced', JSON.stringify(data), {
      headers: {
        'Authorization': `App ${token}`,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }
    });

    return {
      statusCode: 200,
      body: JSON.stringify(result.data)
    };

  } catch (error) {
    console.log(error);
    return {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message)
    }
  }
};

export const main = middyfy(sendSms);
