/* eslint-disable  @typescript-eslint/no-explicit-any */
import { middyfy } from '../../libs/lambda';

import convert from 'xml-js';
import axios from 'axios';
import moment from 'moment';

import fetch from 'node-fetch';

const PATH_NOTIFICATION = '/platform/notifications/actions/notifyUser';
const PATH_USER = '/platform/user/queries/getUser';

const handler = async (event) => {

  console.log('Acionando a g5 para o tenant', event.headers['x-platform-tenant']);

  let server = event.body.server as string;
  const module = event.body.module as string;
  let separator = event.body.separator as string;
  let service = event.body.service as string;
  const port = event.body.port as string;

  let user = event.body.user;
  let password = event.body.password;
  let encryption = event.body.encryption ? event.body.encryption : 0;

  if (!event.headers["x-platform-username"]) {
    // Se não vier a url no header, busca pelo token.
    const responseData = await axios.post(event.headers['x-platform-environment'] + PATH_USER, event.body, {
      headers: { Authorization: event.headers["x-platform-authorization"] }
    });
    event.headers["x-platform-username"] = responseData?.data?.username;
  }

  if (!user || !password) {
    // Se não vier usuário enviar o username / token para a G5.
    user = event.headers["x-platform-username"].split('@')[0];
    password = event.headers["x-platform-authorization"].replace('Bearer ', '').replace('bearer ', '');
    encryption = 3;
  }

  // remove os itens do body para não serem enviados como parâmetros
  delete event.body.service;
  delete event.body.module;
  delete event.body.separator;
  delete event.body.server;
  delete event.body.port;
  delete event.body.user;
  delete event.body.password;
  delete event.body.encryption;

  // valida se o server infomado pelo cliente termina com barra(/)
  if (server[server.length - 1] !== '/') {
    server += '/';
  }

  if (!separator) {
    separator = '_';
  }

  if (!service.includes('?wsdl')) {
    service += '?wsdl';
  }

  for (let i = 0; i < 10; i++) {
    service = service.replace('.', '_');
  }

  // gera a url d D5
  const serviceUrl = `${server}g5-senior-services/${module}${separator}Sync${service}`;

  console.log(serviceUrl);

  const body = {
    user: user,
    password: password,
    encryption: encryption,
    parameters: {}
  };

  body.parameters = event.body;

  let xml = `<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://services.senior.com.br"><soapenv:Body><ser:${port}>${convert.json2xml(JSON.stringify(body), { compact: true, ignoreComment: true, spaces: 0 })}</ser:${port}></soapenv:Body></soapenv:Envelope>`;

  try {
    xml = xml.replace(/(\r\n|\n|\r)/gm, "");
    let response = null as any;
    if (event.headers["x-platform-tenant"] === 'polinutricombr') {
      console.log('aqui');

      const responseHttp = await fetch(serviceUrl, {
        method: 'post',
        body: xml,
        headers: { 'Content-Type': 'text/xml;charset=UTF-8' }
      });
      console.log(responseHttp);
      const data = await responseHttp.text();
      console.log(data);

      response = { data: data };

    } else {
      response = await axios.post(serviceUrl, xml,
        {
          headers:
            { 'Content-Type': 'text/xml;charset=UTF-8' }
        }
      );
    }

    const respondeObject = convert.xml2json(response.data, { compact: true, spaces: 4, alwaysChildren: true });

    let parsed = JSON.parse(respondeObject);
    const keys = Object.keys(parsed);
    const envelope = keys.find(k => k.includes('Envelope'));
    let erro = null;
    if (envelope) {
      const bodyKey = Object.keys(parsed[envelope]).find(k => k.includes('Body'));
      if (bodyKey) {
        const portKey = Object.keys(parsed[envelope][bodyKey]).find(k => k.includes(port));
        if (portKey) {
          const result = parsed[envelope][bodyKey][portKey];
          if (result?.result?.erroExecucao?._text) {
            parsed = {
              message: result?.result?.erroExecucao?._text
            };
            erro = result?.result?.erroExecucao?._text;
          } else {
            const resultKeys = Object.keys(result?.result);

            const newParsed = {} as any;
            resultKeys.forEach(k => {
              if (result?.result[k]._text) {
                newParsed[k] = result?.result[k]?._text;
              }
              if (result?.result[k].length > 0) {
                newParsed[k] = [];
                result?.result[k].forEach(r => {
                  const rKyes = Object.keys(r);
                  if (rKyes.length) {
                    const rObject = {};
                    rKyes.forEach(rk => {
                      if (r[rk]._text) {
                        rObject[rk] = r[rk]?._text;
                      }
                    });
                    newParsed[k].push(rObject);
                  } else {
                    newParsed[k].push(r);
                  }
                });
              } else {
                if (!newParsed[k]) {
                  newParsed[k] = {};
                }
                const rKyes = Object.keys(result?.result[k]);
                rKyes.forEach(rk => {
                  if (result?.result[k][rk]._text) {
                    newParsed[k][rk] = result?.result[k][rk]?._text;
                  }
                });
              }
            });
            parsed = newParsed;
          }
        }
      }
    }

    if (erro) {
      try {
        const notification = {} as any;
        notification.sourceDomain = 'platform';
        notification.sourceService = 'conector';
        notification.destinationUsers = [event.headers["x-platform-username"]];
        notification.notificationOrigin = 'Conector';
        notification.notificationPriority = 'None';
        notification.notificationSubject = 'Erro ao acionar a G5';
        notification.notificationContent = erro;
        notification.notificationKind = 'Management';
        notification.expirationDate = moment().add(1, 'month').toDate();

        await axios.post(event.headers['x-platform-environment'] + PATH_NOTIFICATION, notification, {
          headers: { Authorization: event.headers["x-platform-authorization"] }
        });
      } catch (err) {
        console.log('Não foi possível notificar o usuário', err);
      }
      return {
        statusCode: 400,
        body: JSON.stringify(parsed)
      };
    }

    return {
      statusCode: response?.status || 200,
      body: JSON.stringify(parsed)
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err?.status || err?.response?.status || 200,
      body: JSON.stringify(err.message)
    };
  }


};

export const main = middyfy(handler);
