import { middyfy } from '../../libs/lambda';
import Axios from 'axios';

const authUrl = 'https://rest.megaerp.online/api/Auth/SignIn';

const getCustCentersUrl = 'https://rest.megaerp.online/api/global/CentroCusto';

const handler = async (event) => {

  try {

    const authBody = {
      "userName": event.body.username,
      "password": event.body.password
    }

    const token = await Axios.post(authUrl, authBody, {
      headers: { tenantId: event.body.tenantId }
    });

    const getCustCentersUrlById = `${getCustCentersUrl}/${event.body.id}`

    const custCenters = await Axios.get(getCustCentersUrlById, {
      headers: { Authorization: `Bearer ${token.data.accessToken}` }
    })

    if (custCenters && custCenters.data && custCenters.data.usuarios) {
        delete custCenters.data.usuarios;
    }

    const response = custCenters && custCenters.data ? [custCenters.data] : [];

    return {
      statusCode: 200,
      body: JSON.stringify(response)
    };
  }
  catch (error) {
    console.log(error.response.data);
    return {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message)
    };
  }

};

export const main = middyfy(handler);
