import { APIGatewayProxyHandler } from 'aws-lambda';
import middy from '@middy/core';
import axios from 'axios';
import convert from 'xml-js';
import moment from 'moment';

const handler: APIGatewayProxyHandler = async (event) => {
  console.log('Acionando a g5 para o tenant ', event.headers['x-platform-tenant']);

  let server = event.body.server as string;
  const module = event.body.module as string;
  let separator = event.body.separator as string;
  let service = event.body.service as string;
  const port = event.body.port as string;

  let user = event.body.user;
  let password = event.body.password;
  let encryption = event.body.encryption ? event.body.encryption : 0;

  if (!event.headers["x-platform-username"]) {
    // Se não vier a url no header, busca pelo token.
    const responseData = await axios.post(event.headers['x-platform-environment'] + PATH_USER, event.body, {
      headers: { Authorization: event.headers["x-platform-authorization"] }
    });
    event.headers["x-platform-username"] = responseData?.data?.username;
  }

  if (!user || !password) {
    // Se não vier usuário enviar o username / token para a G5.
    user = event.headers["x-platform-username"].split('@')[0];
    password = event.headers["x-platform-authorization"].replace('Bearer ', '').replace('bearer ', '');
    encryption = 3;
  }

  // remove os itens do body para não serem enviados como parâmetros
  delete event.body.service;
  delete event.body.module;
  delete event.body.separator;
  delete event.body.server;
  delete event.body.port;
  delete event.body.user;
  delete event.body.password;
  delete event.body.encryption;

  // valida se o server infomado pelo cliente termina com barra(/)
  if (server[server.length - 1] !== '/') {
    server += '/';
  }

  if (!separator) {
    separator = '_';
  }

  if (!service.includes('?wsdl')) {
    service += '?wsdl';
  }

  for (let i = 0; i < 10; i++) {
    service = service.replace('.', '_');
  }

  // gera a url d D5
  const serviceUrl = `${server}g5-senior-services/${module}${separator}Sync${service}`;

  console.log(serviceUrl);

  const body = {
    user: user,
    password: password,
    encryption: encryption,
    parameters: {}
  };

  body.parameters = event.body;

  let xml = `<?xml version="1.0" encoding="UTF-8"?>
  <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://services.senior.com.br">
    <soapenv:Body>
      <ser:${port}>
        ${convert.json2xml(JSON.stringify(body), { compact: true, ignoreComment: true, spaces: 4 })}
      </ser:${port}>
    </soapenv:Body>
  </soapenv:Envelope>`;

  try {
    xml = xml.replace(/(\r\n|\n|\r)/gm,"");

    const response = await axios.post(serviceUrl, xml,
      {
        headers:
          { 'Content-Type': 'text/xml' }
      }
    );

    const respondeObject = convert.xml2json(response.data, { compact: true, spaces: 4, alwaysChildren: true });

    let parsed = JSON.parse(respondeObject);
    const keys = Object.keys(parsed);
    const envelope = keys.find(k => k.includes('Envelope'));
    let erro = null;
    if (envelope) {
      const bodyKey = Object.keys(parsed[envelope]).find(k => k.includes('Body'));
      if (bodyKey) {
        const portKey = Object.keys(parsed[envelope][bodyKey]).find(k => k.includes(port));
        if (portKey) {
          const result = parsed[envelope][bodyKey][portKey];
          if (result?.result?.erroExecucao?._text) {
            parsed = {
              message: result?.result?.erroExecucao?._text
            };
            erro = result?.result?.erroExecucao?._text;
          } else {
            const resultKeys = Object.keys(result?.result);
            const newParsed = {};
            resultKeys.forEach(k => {
              if (result?.result[k]?._text) {
                newParsed[k] = result?.result[k]?._text;
              } else if (result?.result[k].length > 0) {
                if (typeof result?.result[k] === 'string') {
                  newParsed[k] = result?.result[k];
                } else {
                  newParsed[k] = parseList(result?.result[k]);
                }
              } else if (result?.result[k]?._attributes && result?.result[k]?._attributes['xsi:nil'] && !result?.result[k]?._text) {
                newParsed[k] = null;
              } else {
                if (JSON.stringify(result?.result[k]) === '{}') {
                  newParsed[k] = '';
                } else {
                  newParsed[k] = parseObject(result?.result[k]);
                }
              }
            });
            parsed = newParsed;
          }
        }
      }
    }

    if (erro) {
      try {
        const notification = {} as any;
        notification.sourceDomain = 'platform';
        notification.sourceService = 'conector';
        notification.destinationUsers = [event.headers["x-platform-username"]];
        notification.notificationOrigin = 'Conector';
        notification.notificationPriority = 'None';
        notification.notificationSubject = 'Erro ao acionar a G5';
        notification.notificationContent = erro;
        notification.notificationKind = 'Management';
        notification.expirationDate = moment().add(1, 'month').toDate();

        await axios.post(event.headers['x-platform-environment'] + PATH_NOTIFICATION, notification, {
          headers: { Authorization: event.headers["x-platform-authorization"] }
        });
      } catch (err) {
        console.log('Não foi possível notificar o usuário', err);
      }
      return {
        statusCode: 400,
        body: JSON.stringify(parsed)
      };
    }

    if (event.body.rootObject) {
      parsed = parsed[event.body.rootObject];
    }

    if (!parsed) {
      parsed = [];
    }

    // Converte o resultado para JSON
    const responseBody = JSON.stringify(parsed);

    return {
      statusCode: response?.status || 200,
      body: responseBody
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err?.status || err?.response?.status || 200,
      body: JSON.stringify(err.message)
    };
  }
};

export const main = middy(handler);
