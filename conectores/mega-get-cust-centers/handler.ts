import { middyfy } from '../../libs/lambda';
import Axios from 'axios';

const authUrl = 'https://rest.megaerp.online/api/Auth/SignIn';

const getCustCentersUrl = 'https://rest.megaerp.online/api/global/CentroCusto';

const handler = async (event) => {

  try {

    const authBody = {
      "userName": event.body.username,
      "password": event.body.password
    }
    
    const token = await Axios.post(authUrl, authBody, {
      headers: { tenantId: event.body.tenantId }
    });
    
    const custCenters = await Axios.get(getCustCentersUrl, {
      headers: { Authorization: `Bearer ${token.data.accessToken}` }
    })

    if (custCenters && custCenters.data && custCenters.data.usuarios) {
        delete custCenters.data.usuarios;
    }

    return {
      statusCode: 200,
      body: JSON.stringify(custCenters.data)
    };
  } 
  catch (error) {
    console.log(error);
    return {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message)
    };
  }

};

export const main = middyfy(handler);