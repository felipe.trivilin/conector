import { middyfy } from '../../libs/lambda';
import { Twilio } from 'twilio';
import { validatePhoneNumber } from '../../libs/utils';

const sendSms = async (event) => {
  try {
    const { destination, accountSid, content, token, providerNumber } = event.body;
    
    const client = new Twilio(accountSid, token);

    const phoneNumber = validatePhoneNumber(destination);

    const data = client.messages.create({
      body: content,
      from: providerNumber,
      to: `+${phoneNumber}`
    });

    return {
      statusCode: 200,
      body: JSON.stringify(data)
    };

  } catch (error) {
    console.log(error);
    return {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message)
    }
  }
};

export const main = middyfy(sendSms);
