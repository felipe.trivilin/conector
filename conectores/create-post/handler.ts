import Axios from 'axios';
import { middyfy } from '../../libs/lambda';

const PATH = '/platform/social/actions/createPost';

const createPost = async (event) => {
  try {

    const url = event.headers['x-platform-environment'] + PATH;

    const responseData = await Axios.post(url, event.body, {
      headers: { Authorization: event.headers["x-platform-authorization"] }
    });

    const response = {
      id: responseData.data.post.id,
      when: responseData.data.post.when,
      author: responseData.data.post.author.name,
      text: responseData.data.post.text,
      approved: responseData.data.post.approved
    }

    return {
      statusCode: responseData?.status || 200,
      body: JSON.stringify(response)
    };

  } catch (error) {
    return {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message)
    }
  }
};

export const main = middyfy(createPost);
