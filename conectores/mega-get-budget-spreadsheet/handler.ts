import { middyfy } from '../../libs/lambda';
import Axios from 'axios';

const authUrl = 'https://rest.megaerp.online/api/Auth/SignIn';

 const getBudgetSpreadsheet = 'https://rest.megaerp.online/api/AdmObra/Planilha'; 

const handler = async (event) => {

  try {
    
    const authBody = {
      "userName": event.body.username,
      "password": event.body.password
    }
    
    const token = await Axios.post(authUrl, authBody, {
      headers: { tenantId: event.body.tenantId }
    });
    
    const getBudgetSpreadsheetUrl = `${getBudgetSpreadsheet}/${event.body.codigoOrcamento}/${event.body.sequenciaOrcamento}/${event.body.tipoPlanilha}}`

    const budgetSpreadsheet = await Axios.get(getBudgetSpreadsheetUrl, {
      headers: { Authorization: `Bearer ${token.data.accessToken}` }
    })

    if (budgetSpreadsheet && budgetSpreadsheet.data && budgetSpreadsheet.data.usuarios) {
        delete budgetSpreadsheet.data.usuarios;
    }

    return {
      statusCode: 200,
      body: JSON.stringify(budgetSpreadsheet.data)
    };
  } 
  catch (error) {
    console.log(error.response.data);
    return {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message)
    };
  }

};

export const main = middyfy(handler);