/* eslint-disable  @typescript-eslint/no-explicit-any */
import { middyfy } from '../../libs/lambda';

import Axios from 'axios';

const novasoftGenericApiHttp = async (event) => {

  try {
    const endpointLogin = '/api/Cuenta/login';
    let body = event.body;
    
    const url = body.url;
    const method = body.method ? body.method.toLowerCase() : 'post';
    const userLogin = body.userLogin;
    const password = body.password;
    const connectionName = body.connectionName;
    const endpoint = body.endpoint;

    const PATH = url + endpoint;
    const PATH_LOGIN = url + endpointLogin;

    const loginData = { userLogin, password, connectionName};

    const token = await Axios.post(PATH_LOGIN, loginData, {});
    const headers = { authorization: 'Bearer ' + token.data.token };

    if (body.customHeader) {
      const custom_header = body.custom_header;
      const arr_custom = custom_header.split(';');
      for (let i = 0; i < arr_custom.length; i++) {
        const header = arr_custom[i].split(':');
        headers[header[0]] = header[1];
      }
      delete body.customHeader;
    }

    delete body.url;
    delete body.method;
    let responseData = null as any;
    if (body.contents) {
        body = [body.contents];
    }
    if (method === 'post') {
      responseData = await Axios.post(PATH, body, {
        headers: headers
      });
    } else if (method === 'get') {
      responseData = await Axios.get(PATH, {
        params: body,
        headers: headers
      });
    } else if (method === 'put') {
      responseData = await Axios.put(PATH, body, {
        headers: headers
      });
    } else {
      responseData = await Axios.delete(PATH, {
        params: body,
        headers: headers
      });
    }

    let response = responseData.data;

    if (event.body.rootObject) {
      response = response[event.body.rootObject];
    }

    return {
      statusCode: responseData?.status || 200,
      body: JSON.stringify(response)
    };

  } catch (error) {
    console.log(error);
    return {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message)
    }
  }
};


export const main = middyfy(novasoftGenericApiHttp);
