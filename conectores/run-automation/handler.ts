import Axios from 'axios';
import { middyfy } from '../../libs/lambda';

const PATH = '/platform/rpa/queries/runAutomation';

const runAutomation = async (event) => {
  try {

    const url = event.headers['x-platform-environment'] + PATH;

    const responseData = await Axios.get(`${url}?id=${event.body.id}`, {
      headers: { Authorization: event.headers["x-platform-authorization"] }
    });

    return {
      statusCode: responseData?.status || 200,
      body: JSON.stringify(responseData?.data)
    };

  } catch (error) {
    console.log(error);
    return {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message)
    }
  }
};

export const main = middyfy(runAutomation);
