import axios from 'axios';
import {middyfy} from '../../libs/lambda';

const getBpmAllDocumentsTickets = '/platform/workflow/queries/getAllDocumentTickets';
const getDocsId = '/platform/ecm_ged/queries/newDocumentStatus';
const getDocsVersion = '/platform/ecm_ged/queries/getDocumentVersions';
const saveEnvelope = '/platform/ecm_ged/actions/saveEnvelope';
const sendEnvelopeDraftToSign = '/platform/ecm_ged/actions/sendDraftToSign';
const notifyUserEnvelope = '/platform/notifications/actions/notifyUser';
const getUser = '/platform/user/queries/getUser';

const bpmDocsEnvelope = async (event) => {

    const {processInstanceId, instructionsToSigner, signerName, signerEmail, envelopeName} = event.body;

    const environmentUrl = event.headers['x-platform-environment'];
    const authorizationToken = event.headers['x-platform-authorization'];

    const getBpmAllDocumentsTicketsUrl = `${environmentUrl}${getBpmAllDocumentsTickets}`;
    const getDocsIdUrl = `${environmentUrl}${getDocsId}`;
    const getDocsVersionUrl = `${environmentUrl}${getDocsVersion}`;
    const getUserUrl = `${environmentUrl}${getUser}`;
    const saveEnvelopeUrl = `${environmentUrl}${saveEnvelope}`;
    const sendEnvelopeDraftToSignUrl = `${environmentUrl}${sendEnvelopeDraftToSign}`;
    const notifyUserEnvelopeUrl = `${environmentUrl}${notifyUserEnvelope}`

    const notifyUser = async (envelopeId: string) => {

        const getUserResponse = await axios.get(getUserUrl, {
            headers: {Authorization: authorizationToken}
        });

        const notifyUserEnvelopeCreatedBody = {
            sourceDomain: 'platform',
            sourceService: 'conector',
            notificationOrigin: 'conector',
            notificationPriority: 'None',
            notificationKind: 'Management',
            destinationUsers: [`${getUserResponse.data.username}@${getUserResponse.data.tenantDomain}`],
            notificationSubject: 'Conector Criar Envelope BPM',
            notificationContent: `Envelope ID - ${envelopeId}`
        }

        await axios.post(notifyUserEnvelopeUrl, notifyUserEnvelopeCreatedBody, {
            headers: {Authorization: authorizationToken}
        });
    }

    const sleep = (ms: number) => new Promise((r) => setTimeout(r, ms));

    const validateStringArrayIsEmpty = async (array: string[]) => {
        if (!array.length) {
            return {
                statusCode: 200,
                body: JSON.stringify({})
            };
        }
    }

    await sleep(20000);

    try {

        let documentsTicket: string[] = [];
        const getBpmAlDocumentsTickets = await axios.get(`${getBpmAllDocumentsTicketsUrl}?processInstanceId=${processInstanceId}`, {
            headers: {Authorization: authorizationToken}
        });

        documentsTicket = getBpmAlDocumentsTickets.data.tickets;
        await validateStringArrayIsEmpty(documentsTicket);

        const documentsId: string[] = []
        for (const ticket of documentsTicket) {
            if (ticket && ticket != 'undefined') {
                const getDocsId = await axios.get(`${getDocsIdUrl}?ticket=${ticket}`, {
                    headers: {Authorization: authorizationToken}
                });
                documentsId.push(getDocsId.data.documentId);
            }
        }

        await validateStringArrayIsEmpty(documentsId);

        const documentVersionIds: string[] = [];
        for (const documentId of documentsId) {
            if (documentId && documentId != 'undefined') {
                const getDocsVersion = await axios.get(`${getDocsVersionUrl}?documentId=${documentId}`, {
                    headers: {Authorization: authorizationToken}
                });
                documentVersionIds.push(getDocsVersion.data.documentVersions[0].id);
            }
        }

        await validateStringArrayIsEmpty(documentVersionIds)

        const saveEnvelopeBody = {
            envelopeDraftId: '',
            name: envelopeName,
            daysToNotify: 0,
            daysToExpire: 120,
            permissions: [],
            documentsVersions: documentVersionIds,
            signers: [{
                name: signerName,
                email: signerEmail,
            }],
            instructionsToSigner,
            askGeolocation: 'DONT_ASK_LOCATION',
        };

        const saveEnvelopeDraft = await axios.post(saveEnvelopeUrl, saveEnvelopeBody, {
            headers: {Authorization: authorizationToken}
        });

        const signEnvelopeDraftBody = {
            envelopeDraftId: saveEnvelopeDraft.data.envelopeDraftId,
            notifyUser: true,
            sendEmail: true
        };

        const signEnvelopeDraft = await axios.post(sendEnvelopeDraftToSignUrl, signEnvelopeDraftBody, {
            headers: {Authorization: authorizationToken}
        });

        await notifyUser(signEnvelopeDraft.data.envelopeId);

        return {
            statusCode: signEnvelopeDraft?.status || 200,
            body: JSON.stringify(signEnvelopeDraft?.data)
        };

    } catch (error) {
        return {
            statusCode: error?.response?.status || 400,
            body: JSON.stringify(error?.response?.data ? error?.response?.data : error.message)
        }
    }
};

export const main = middyfy(bpmDocsEnvelope);
