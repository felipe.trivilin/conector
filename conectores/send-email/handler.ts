/* eslint-disable  @typescript-eslint/no-explicit-any */
import Axios from 'axios';
import { middyfy } from '../../libs/lambda';

const PATH = '/platform/email/actions/sendEmail';

const sendEmail = async (event) => {
  try {

    const url = event.headers['x-platform-environment'] + PATH;

    let to = [];
    if (event.body?.destinations) {
      try {
        to = JSON.parse(event.body?.destinations);
      } catch (err) {
        if (event.body?.destinations.split(',').length) {
          to = event.body?.destinations.trim().split(',');
        } else {
          to = [event.body?.destinations] as any;
        }
      }
    }

    const body = {
      destination: {
        to: to
      },
      content: {
        body: { 
          text: event.body?.content
        },
        subject: event.body?.subject
      }
    }

    const responseData = await Axios.post(url, body, {
      headers: { Authorization: event.headers["x-platform-authorization"] }
    });

    return {
      statusCode: responseData?.status || 200,
      body: JSON.stringify(responseData?.data)
    };

  } catch (error) {
    console.log(error);
    return {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message)
    }
  }
};

export const main = middyfy(sendEmail);
