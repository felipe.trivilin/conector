import { middyfy } from '../../libs/lambda';
import Axios from 'axios';

const PATH = '/hcm/payroll/entities/person/';
const URL_HCM = 'https://hcm-api.senior.com.br/frontend-api/profile';

const createResponse = (data) => {
  return {
    name:
      data?.name ||
      `${data?.firstname} ${data?.middlename} ${data?.lastname}`,
    firstName: data?.firstname,
    middleName: data?.middlename,
    lastName: data?.lastname,
    username: data?.username,
    cpf: data?.cpf,
    race: data?.race,
    nickname: data?.nickname,
    gender: data?.gender,
    maritalStatus: data?.maritalStatus || data?.maritalstatus,
    birthday: data?.birthday,
    nationalityId: data?.nationality?.id,
    nationalityName: data?.nationality?.name,
    nationalityCode: data?.nationality?.code,
    isDisabledPerson: data?.isDisabledPerson || data?.isdisabledperson,
    placeOfBirthId: data?.placeOfBirth?.id || data?.placeofbirth?.id,
    placeOfBirthName: data?.placeOfBirth?.name || data?.placeofbirth?.name,
    placeOfBirthStateId:
      data?.placeOfBirth?.state?.id || data?.placeofbirth?.stateId?.id,
    placeOfBirthStateName:
      data?.placeOfBirth?.state?.name || data?.placeofbirth?.stateId?.name,
    placeOfBirthStateAbbreviation:
      data?.placeOfBirth?.state?.abbreviation ||
      data?.placeofbirth?.stateId?.abbreviation,
    placeOfBirthCountryId:
      data?.placeOfBirth?.state?.country?.id ||
      data?.placeofbirth?.stateId?.countryId?.id,
    placeOfBirthCountryName:
      data?.placeOfBirth?.state?.country?.name ||
      data?.placeofbirth?.stateId?.countryId?.name,
    placeOfBirthCountryAbbreviation:
      data?.placeOfBirth?.state?.country?.abbreviation ||
      data?.placeofbirth?.stateId?.countryId?.abbreviation
  };
}

const hcmGetPerson = async (event) => {
  const personId = event?.body?.personId;
  const authorization = event.headers['x-platform-authorization']
    .trim()
    .replace('bearer', '');

  try {
    if (!personId) {
      const accessToken = {
        access_token: authorization
      };

      const cookie = encodeURI(
        `com.senior.token=${JSON.stringify(accessToken)}`
      );

      const response = await Axios.get(URL_HCM, {
        headers: {
          cookie: cookie
        },
      });

      const data = response.data;

      const result = createResponse(data);

      return {
        statusCode: response?.status || 200,
        body: JSON.stringify(result)
      };
    }

    const url = event.headers['x-platform-environment'] + PATH + personId;

    const response = await Axios.get(url, {
      headers: {
        Authorization: event.headers['x-platform-authorization'],
      }
    });

    const data = response.data;

    const result = createResponse(data);

    return {
      statusCode: response?.status || 200,
      body: JSON.stringify(result)
    };
  } catch (error) {
    console.log(error);
    return {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data
        ? JSON.stringify(error?.response?.data)
        : JSON.stringify(error.message)
    };
  }
};

export const main = middyfy(hcmGetPerson);
